FROM alpine

RUN apk --update add \
  python \
  curl \
  groff

RUN curl https://bootstrap.pypa.io/get-pip.py | python && \
  pip install awscli

RUN addgroup -g 1000 ciuser && \
    adduser -D -u 1000 -G ciuser ciuser

USER ciuser

CMD ["tail", "-f", "/dev/null"]  
