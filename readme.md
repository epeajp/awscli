# jenkins用aws cli
## usage
Jenkinsfile内のエージェントで以下の様に使用
```
agent {
  docker {
    image 'kyoshitake/awscli'
    args '-v /path/to/awsconfigs:/home/ciuser:rw -e HOME=/home/ciuser'
  }
}       
```
/path/to/awsconfigsは.awsのあるディレクトリ   
